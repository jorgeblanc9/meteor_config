# Scaffold Meteor + Vue

## Description

Web app built with Meteor and Vue, include:
- Authentication
- Users management
- Chat


Supported Platforms
-------------------

- macOS 10.12+
- Linux
- Windows 10

System Requirements
-------------------

- Node 12+ [download here](https://nodejs.org/es/download/)
- Yarn 1.22.0+
- Meteor 2.0 [Installation](https://www.meteor.com/install)
- Mongo 4.2.5 and Robo 3T [download here](https://www.mongodb.com/download-center/community)
- **Note:** In some cases it is necesary to disable antivrus in order to works SMTP Server

## Installation

- Clone the last version of repository [here](https://github.com/<your_username>/<your_repo_name>.git)
- Verify you are on branch **dev**

**Database configuration with terminal**

Note: For Windows Systems verify that mongo is configured as environment system variable.

1. Open a terminal in the project root
2. Restore the database with:
```shell
mongorestore --db curso-meteor ./database/curso-meteor
```

**The following commands are only for support:**

- Export a backup of the database (data exported as formats BSON and JSON):
```shell
mongodump --db curso-meteor --out ./database/
```

- Import more data of a specific collection to the database:
```shell
mongoimport --db curso-meteor --collection <collection_name> <path_collection_json>
```

- Export data of a specific collection from the database:
```shell
mongoexport --db curso-meteor --collection <collection_name>
```

- Restore backup of database from external database:
```shell
mongorestore --host <HOST> --port <PORT> --username=<USERNAME> --password=<PASSWORD> --authenticationDatabase curso-meteor --db <DATABASE> ./database/curso-meteor
```

- Export backup of database from external database:
```shell
mongodump --host <HOST> --port <PORT> --username=<USERNAME> --password=<PASSWORD> --authenticationDatabase curso-meteor --db <DATABASE> --out ./path_to_save
```

**Installing dependencies**

- Run the following commands to install the dependencies:
```shell
yarn
```

**Configure environment variables file**

Go to `./settings/` and copy `settings-development-example.json` to `settings-development.json` and modify the following:

- **STORAGE_PATH** to your path project in your PC/MAC.

Running project
---------------

**On Mac OS X and Linux**
- Run the following commands:
```shell
yarn run:mac
```

**On Windows**
- Run the following commands:
```shell
SET MONGO_URL=mongodb://localhost:27017/curso-meteor
yarn run:windows
```


**Note:**
You can configure your Jetbrains IDE to run the project from the IDE execution button.


## Installing prerequisites for mobile devices
- For Android:
    - Install JDK [here](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) 
    - Install Android Studio [here](https://developer.android.com/studio)
    - Download API 28 from SDK Manager 
    - Android SDK Build Tools (latest)
    - Android SDK Platform Tools (latest)
    - Install Gradle
        - On Mac use: `brew install gradle`
        - On Windows use this [guide](http://bryanlor.com/blog/gradle-tutorial-how-install-gradle-windows)
        - On Debian/Ubuntu use: `sudo apt-get install gradle`

- For iOS (only for Mac):
    - Install XCode
    - Enable XCode CLT with `sudo xcode-select -s /Applications/Xcode.app/Contents/Developer` 
    - Install Cocoa Pods with `sudo gem install cocoapods`
 
- You must have set the system variables:

####Configure environment variables for Mac:

*File of environment variables is located on ~/ (user home directory) and the filename could be .zshrc, .bash_profile or .bashrc).
For Mac OS 11 Big sur the filename is **.zshrc** 
````
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_261.jdk/Contents/Home
export ANDROID_HOME=$HOME/Library/Android/sdk   (DEPRECATED)
export ANDROID_SDK_ROOT=$ANDROID_HOME           (RECOMMENDED)
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
````
Save file and execute: `source ~/.zshrc`


####Configure environment variables for Windows:

````
set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_231
set ANDROID_HOME=C:\Users\HEC_I\AppData\Local\Android\Sdk   (DEPRECATED)
set ANDROID_SDK_ROOT=$ANDROID_HOME                          (RECOMMENDED)
set PATH=%PATH%;%JAVA_HOME%\bin;%ANDROID_HOME%\platform-tools;C:\gradle-6.2.1\bin
````

Once all this done: Restart your PC.
